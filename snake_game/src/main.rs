extern crate glutin_window;
extern crate graphics;
extern crate opengl_graphics;
extern crate piston;
extern crate rand;

use glutin_window::GlutinWindow;
use opengl_graphics::{GlGraphics, OpenGL};
use piston::event_loop::*;
use piston::input::*;
use piston::window::WindowSettings;

use std::collections::LinkedList;
use std::iter::FromIterator;

#[derive(Clone, PartialEq)]
enum Direction {
    Right, Left, Up, Down
}

struct Game {
    gl: GlGraphics,
    snake: Snake,
    enemy: Enemy,
    food: Food,
    life: bool,
    score: u32,
    
}

impl Game {
    fn render(&mut self, args: &RenderArgs) {
        use graphics::*;

        const GREEN: [f32; 4] = [0.0, 1.0, 0.0, 1.0];

        self.gl.draw(args.viewport(), |c, gl| {
            // Clear the screen.
            graphics::clear(GREEN, gl);
        });

        self.snake.render(&mut self.gl, args);
        self.food.render(&mut self.gl, args);
        self.enemy.render(&mut self.gl, args);
    }

    fn update(&mut self) {
        self.snake.update(&mut self.food, &mut self.life, &mut self.score);
        self.enemy.update();
        
    }

    fn pressed(&mut self, btn: &Button) {
        let last_direction = self.snake.dir.clone();

        self.snake.dir = match btn {
            &Button::Keyboard(Key::Up)
                if last_direction != Direction::Down => Direction::Up,
            &Button::Keyboard(Key::Down)
                if last_direction != Direction::Up => Direction::Down,
            &Button::Keyboard(Key::Left)
                if last_direction != Direction::Right => Direction::Left,
            &Button::Keyboard(Key::Right)
                if last_direction != Direction::Left => Direction::Right,
            _ => last_direction
        }
    }
}


struct Snake {
    body: LinkedList<(i32, i32)>,
    dir: Direction,
    meet_food: bool,
}

#[derive(Clone, Copy)]
struct Food {
    loc: (i32, i32),
}

struct Enemy {
    body: (i32,i32),
    dir: Direction,
}

impl Snake {
    fn render(&self, gl: &mut GlGraphics, args: &RenderArgs) {
        use graphics;

        const RED: [f32; 4] = [1.0, 0.0, 0.0, 1.0];

        let squares: Vec<graphics::types::Rectangle> = self.body
            .iter()
            .map(|&(x,y)| {
                graphics::rectangle::square(
                    (x*20) as f64,
                    (y*20) as f64,
                    20_f64)
            })
            .collect();

        gl.draw(args.viewport(), |c,gl| {
            let transform = c.transform;
            squares.into_iter()
                .for_each(|square| graphics::rectangle(RED, square, transform, gl));
        });
    }

    fn collide(head: (i32, i32), body: &LinkedList<(i32,i32)>, life: &mut bool) {
        for pos in body.iter() {
            if *pos == head {
                *life = false;
            }
        }

    }

    fn update(&mut self, food: &mut Food, life: &mut bool, score: &mut u32) {
        use rand::Rng;

        let mut new_head = (*self.body.front().expect("Snake has no body")).clone();

        match self.dir {
            Direction::Down => new_head.1 += 1,
            Direction::Up => new_head.1 -=1,
            Direction::Left => new_head.0 -= 1,
            Direction::Right => new_head.0 += 1,
        }

        
        if new_head.0 == 20 {
            new_head.0 = 0;
        }
        else {
            if new_head.0 == -1 {
                new_head.0 = 20;
            }
        }

        if new_head.1 == 20 {
            new_head.1 = 0;
        }
        else {
            if new_head.1 == -1 {
                new_head.1 = 20;
            }
        }

        
        Snake::collide(new_head, &self.body, life);

        self.body.push_front(new_head);

        if food.loc.0 != new_head.0 || food.loc.1 != new_head.1 {
            self.body.pop_back().unwrap();
            self.meet_food = false;
        }
        else {
            self.meet_food = true; 
            *score += 1;
            let mut rng = rand::thread_rng();
            food.loc = (rng.gen_range(0..=10) , rng.gen_range(0..=10));
        }
        

    }
}

impl Food {
    fn render(&self, gl: &mut GlGraphics, args: &RenderArgs) {
        use graphics;

        const  BLUE: [f32; 4] = [0.0, 0.0, 1.0, 1.0];
        let square = graphics::rectangle::square(
            (self.loc.0*20) as f64,
            (self.loc.1*20) as f64,
            20_f64);
        gl.draw(args.viewport(), |c,gl|{
            let transform = c.transform;
            graphics::rectangle(BLUE, square, transform, gl)
            
        });
    }
}

impl Enemy {

    fn render(&self, gl: &mut GlGraphics, args: &RenderArgs) {
        use graphics;
        
        const  WHITE: [f32; 4] = [1.0, 1.0, 1.0, 1.0];
        let square = graphics::rectangle::square(
            (self.body.0*20) as f64,
            (self.body.1*20) as f64,
            20_f64);
        gl.draw(args.viewport(), |c,gl|{
            let transform = c.transform;
            graphics::rectangle(WHITE, square, transform, gl)
            
        });
    }

    fn update(&mut self) {

        // let mut new_head = self.body.clone();

        // match self.dir {
        //     Direction::Down => new_head.1 += 1,
        //     Direction::Up => new_head.1 -=1,
        //     Direction::Left => new_head.0 -= 1,
        //     Direction::Right => new_head.0 += 1,
        // }

        // self.body = new_head;

        match self.dir {
            Direction::Down => self.body.1 += 1,
            Direction::Up => self.body.1 -=1,
            Direction::Left => self.body.0 -= 1,
            Direction::Right => self.body.0 += 1,
        }
    }

}

fn main() {

    let opengl = OpenGL::V3_2;

    let mut window: GlutinWindow = WindowSettings::new("Snake Game",[400, 400])
        .graphics_api(opengl)
        .exit_on_esc(true)
        .build()
        .unwrap();
    

    let mut game = Game {
        gl: GlGraphics::new(opengl),
        snake: Snake {
            body: LinkedList::from_iter((vec![(0,0), (0,1)]).into_iter()),
            dir: Direction::Right,
            meet_food: false,
        },
        enemy: Enemy { body: (20,5), dir: Direction::Left },
        food: Food{loc: (5,5)},
        life: true,
        score: 0_u32,

    };


    let mut events = Events::new(EventSettings::new()).ups(8).max_fps(60);

    while let Some(event) = events.next(&mut window) {
        
        if let Some(ren) = event.render_args() {
            if game.life == true {
                game.render(&ren);
            }
            else {
                println!("You scored {} points", game.score);
                break
            }
            
        }

        if let Some(u) = event.update_args() {
            game.update();
        }

        if let Some(k) = event.button_args() {
            if k.state == ButtonState::Press {
                game.pressed(&k.button);
            }
        }
    }
}
