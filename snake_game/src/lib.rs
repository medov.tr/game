#![no_std]

use gstd::{msg, prelude::*, ActorId};
use game_io::*;
use ft_io::*;

#[derive(Debug, Default)]
pub struct GameSnake {
    pub game_id: ActorId,
    pub ft_cotract_id: ActorId,
    pub player: ActorId,
    pub bid: u64,
    pub game_is_on: bool,
}

static mut GAME: Option<GameSnake> = None;
// const ZERO_ID: ActorId = ActorId::new([0u8; 32]);

impl GameSnake {
    async fn start(&mut self, game_id: &ActorId, ft_contract_id: &ActorId, bid: u64) {
        // TODO:
        // let check_player = msg::send_for_reply_as...

        if self.game_is_on{
            panic!("Game already exists")
        }

        if bid != 1000 {
            panic!("Your bid must be 1000")
        }

        self.game_id = *game_id;
        self.player = msg::source();
        self.ft_cotract_id = *ft_contract_id;
        self.bid = bid;
        self.game_is_on = true;


        msg::send_for_reply_as::<_, FTEvent>(
            self.ft_cotract_id,
            FTAction::Transfer { from: msg::source(), to: self.game_id, amount: self.bid.into() },
            0,
        )
        .expect("Error in sending async message [FTAction::Transfer]")
        .await
        .expect("Error in async message [FTAction::Transfer]");
        
        msg::reply(GameEvent::Started { game_id: self.game_id, player: self.player },0)
        .expect("Error in reply [GameEvent::GameStarted]");
    }

    async fn finish (&mut self, score: u64) {
        let price = score*10;

        msg::send_for_reply_as::<_, FTEvent>(
            self.ft_cotract_id,
            FTAction::Transfer { from: self.game_id, to: msg::source(), amount: price.into() },
            0,
        )
        .expect("Error in sending async message [FTAction::Transfer]")
        .await
        .expect("Error in async message [FTAction::Transfer]");


        msg::reply(GameEvent::Finished { price: price },0)
        .expect("Error in reply [GameEvent::GameStarted]");

    }

}

#[gstd::async_main]
async unsafe fn main() {

    let action: GameAction = msg::load().expect("Could not load Action");
    let game: &mut GameSnake = unsafe {GAME.get_or_insert(GameSnake::default())};

    match action {
        GameAction::Start {
            game_id,
            ft_contract_id,
            bid,
        } => game.start(&game_id, &ft_contract_id, bid).await,
        GameAction::Finish { score } => game.finish(score).await,
    }

}