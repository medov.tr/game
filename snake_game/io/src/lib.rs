#![no_std]

use codec::{Decode, Encode};
use gstd::{prelude::*, ActorId};
use scale_info::TypeInfo;

#[derive(Debug, Encode, Decode, TypeInfo)]

pub enum GameAction {
    Start {
        game_id: ActorId,
        ft_contract_id: ActorId,
        bid: u64,
    },
    Finish{ 
        score: u64
    }
    
}

#[derive(Debug, Encode, Decode, TypeInfo)]
pub enum GameEvent {
    Started{
        game_id: ActorId,
        player: ActorId,

    }, 
    Finished{
        price: u64
    }


}
