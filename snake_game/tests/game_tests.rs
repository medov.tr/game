use codec::Encode;
use game_io::*;
mod utils; 
pub use utils::*;
use gtest::{Program, System};
use ft_io::*;

fn init_game(sys: & System){
    let game = Program::current(sys);
    let res = game.send(USERS[0], GameAction::Start { game_id: 1.into(), ft_contract_id: 2.into(), bid: 0 });
    assert!(res.log().is_empty());
}

#[test]
fn make_bid_game_success(){
    let sys = System::new();
    sys.init_logger();
    init_game(&sys);
    init_ft(&sys);
    let game = sys.get_program(1);
    let ft = sys.get_program(2);
    ft.send(USERS[1], FTAction::Mint {to: USERS[0].into(), amount: 1000.into()});
    let res = game.send(USERS[0], GameAction::Start { game_id: 1.into(), ft_contract_id: 2.into(), bid: 1000 });

    assert!(res.contains(&(
        USERS[0],
        GameEvent::Started { game_id: 1.into(), player: USERS[0].into() }
        .encode()
    )));
}