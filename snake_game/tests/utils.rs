// use codec::{Decode, Encode};
use gtest::{Program, System};
use ft_io::*;

pub const USERS: &'static [u64] = &[5, 6, 7, 8];
pub const GAME_ID: u64 = 2;
pub const BID: u64 = 1000;

pub fn init_ft(sys: &System) {
    sys.init_logger();
    let ft = Program::from_file(
        &sys,
        "../ft/target/wasm32-unknown-unknown/release/fungible_token.wasm",
    );

    assert!(ft
        .send(
            USERS[0],
            InitFT {
                name: String::from("MyToken"),
                symbol: String::from("MTK"),
            },
        )
        .log()
        .is_empty());
}